## WPezClasses: Custom Post Type Labels (US)

__Create your WordPress custom post type labels The ezWay.__

This particular class is for English (US). 


### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._

See the full example in the CPTRegister README. Link in the Helpful Links (below).


There are four public methods:

- setNameSingular( $str_singular ) - Sets the name singular

- setNamePlural( $str_plural ) - Sets the name plural

- setNames( $str_singular, $str_plural ) - Sets both via a single method. If the plural is not passed, the method will automatically append a 's' to the $str_singular that is passed. 

- getLabels() - Once you're done setting, then get the labels.


### FAQ

__1) Why?__

Using the WordPress custom post type labels' defaults is a not-so-good UX, and crafting them by hand is time consuming. This solves both of those problems.   

__2) Why not use a i18n language file?__

Aside from adding friction, langauge translation - in the context of UX - can be more involved than phrase for phrase matching / substitution. This keeps it simple and adds some flexibility.

__3) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __4) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

- https://gitlab.com/wpezsuite/WPezClasses/WPezAutoload

- https://gitlab.com/wpezsuite/WPezClasses/CPTArgs

- https://gitlab.com/wpezsuite/WPezClasses/CPTRegister

- https://codex.wordpress.org/Function_Reference/register_post_type



### TODO

- Add support for: "Note: To set labels used in post type admin notices, see the ‘post_updated_messages’ filter", as seen here:
https://developer.wordpress.org/reference/functions/get_post_type_labels/


- 


### CHANGE LOG

- v0.0.3 - Sun 8 Jan 2023
  - Refactoring + doc comments

- v0.0.2 - Wednesday 30 November 2022
     Refactored + doc comment'ed. Added any labels that we missing from the previous version(s).

- v0.0.1 - Monday 15 April 2019
   This has been in development for quite some time, but not independently repo'ed until now. Please pardon the delay. 