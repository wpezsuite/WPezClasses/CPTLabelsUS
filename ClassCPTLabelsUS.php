<?php
/**
 * ClassCPTLabelsUS.php
 *
 * @package WPezSuite\WPezClasses\CPTLabelsUS;
 */

namespace WPezSuite\WPezClasses\CPTLabelsUS;

// No WP? No go.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

/**
 * "Automates" the creation of the labels array for a custom post type.
 */
class ClassCPTLabelsUS {

	/**
	 * CTP name singular
	 *
	 * @var string
	 */
	protected $str_name_singular;

	/**
	 * CTP name plural.
	 *
	 * @var string
	 */
	protected $str_name_plural;

	/**
	 * Setters return nothing but the result of a given set is available via this property and getSetReturn().
	 *
	 * @var mixed
	 */
	protected $mix_set_return;


	/**
	 * Class constructor.
	 */
	public function __construct() {

		$this->setPropertyDefaults();
	}

	/**
	 * Sets the class defaults.
	 */
	protected function setPropertyDefaults() {

		$this->str_name_singular = false;
		$this->str_name_plural   = false;
		$this->mix_set_return    = null;
	}

	/**
	 * Returns the current value of the protected property $mix_set_return.
	 *
	 * @return mixed
	 */
	public function getSetReturn() {

		return $this->mix_set_return;
	}

	/**
	 * Set the str_name_singular property.
	 *
	 * @param string $str The singular name of the CPT.
	 *
	 * @return void
	 */
	public function setNameSingular( string $str = '' ) {

		$this->mix_set_return = $this->setNameMaster( 'str_name_singular', $str );
	}

	/**
	 * Set the str_name_plural property.
	 *
	 * @param string $str The plural name of the CPT.
	 *
	 * @return void
	 */
	public function setNamePlural( string $str = '' ) {

		$this->mix_set_return = $this->setNameMaster( 'str_name_plural', $str );
	}

	/**
	 * Master setter for the str_name_singular and str_name_plural properties.
	 *
	 * @param string $str_prop The property to set.
	 * @param string $str_val The value to set.
	 */
	protected function setNameMaster( string $str_prop, string $str_val ) {

		$str = esc_attr( $str_val );
		if ( is_string( $str_val ) && ! empty( $str_val ) ) {

			$this->$str_prop = $str_val;
			return true;
		}

		return false;
	}

	/**
	 * Sets both name properties.
	 *
	 * @param string $str_singular The singular name of the CPT.
	 * @param string $str_plural The plural name of the CPT.
	 *
	 * @return void
	 */
	public function setNames( string $str_singular = '', string $str_plural = '' ) {

		if ( empty( $str_singular ) ) {
			$this->mix_set_return = false;
		} else {

			if ( empty( $str_plural ) ) {
				$str_plural = $this->makePlural( $str_singular );
			}
			$this->setNameSingular( $str_singular );
			$this->setNamePlural( $str_plural );

			$this->mix_set_return = true;
		}
	}

	/**
	 * Adds an 's' to the end of a string.
	 *
	 * @param string $str The string to pluralize.
	 *
	 * @return string
	 */
	protected function makePlural( string $str = '' ) {

		return $str . 's';
	}

	/**
	 * Returns the labels array for a CPT.
	 *
	 * @return array
	 */
	public function getLabels() {

		// Once the singular_name and plural_name are set, everything else is taken care of.
		if ( false === $this->str_name_singular || false === $this->str_name_plural ) {
			return array();
		}

		$arr_labels = array(
			// 'name' - general name for the post type, usually plural. The same and overridden by $post_type_object->label. Default is Posts/Pages
			'name'                     => $this->str_name_plural,

			// Name for one object of this post type. Default is Post/Page.
			'singular_name'            => $this->str_name_singular,

			// the add new text. The default is "Add New" for both hierarchical and non-hierarchical post types. When internationalizing this string,
			// please use a gettext context matching your post type. Example: _x('Add New', 'product'); .
			'add_new'                  => 'Add New',

			// Default is Add New Post/Add New Page.
			'add_new_item'             => 'Add New ' . $this->str_name_singular,

			// Default is Edit Post/Edit Page.
			'edit_item'                => 'Edit ' . $this->str_name_singular,

			// Default is New Post/New Page.
			'new_item'                 => 'New ' . $this->str_name_singular,

			// Default is View Post/View Page.
			'view_item'                => 'View ' . $this->str_name_singular,

			// Label for viewing post type archives. Default is 'View Posts' / 'View Pages'.
			'view_items'               => 'View ' . $this->str_name_plural,

			// Default is Search Posts/Search Pages.
			'search_items'             => 'Search ' . $this->str_name_plural,

			// Default is No posts found/No pages found.
			'not_found'                => 'Sorry. Search found no ' . $this->str_name_plural,

			// Default is No posts found in Trash/No pages found in Trash.
			'not_found_in_trash'       => 'Not found in Trash',

			// This string isn't used on non-hierarchical types. In hierarchical ones the default is 'Parent Page:'.
			'parent_item_colon'        => $this->str_name_singular . ':',

			// String for the submenu. Default is All Posts/All Pages.
			'all_items'                => 'All ' . $this->str_name_plural,

			// String for use with archives in nav menus. Default is Post Archives/Page Archives.
			'archives'                 => $this->str_name_singular . ' Archives',

			// Label for the attributes meta box. Default is 'Post Attributes' / 'Page Attributes'.
			'attributes'               => $this->str_name_singular . ' Attributes',

			// String for the media frame button. Default is Insert into post/Insert into page.
			'insert_into_item'         => 'Insert into this ' . $this->str_name_singular,

			// String for the media frame filter. Default is Uploaded to this post/Uploaded to this page.
			'uploaded_to_this_item'    => 'Uploaded to this ' . $this->str_name_singular,

			// Default is Featured Image.
			'featured_image'           => 'Featured Image',

			// Default is Set featured image.
			'set_featured_image'       => 'Set Featured Image',

			// Default is Remove featured image.
			'remove_featured_image'    => 'Remove Featured Image',

			// Default is Use as featured image.
			'use_featured_image'       => 'Use as Featured Image',

			// Default is the same as `name`.
			'menu_name'                => $this->str_name_plural,

			// String for the table views hidden heading.
			'filter_items_list'        => $this->str_name_plural . ': Filter list',

			// Label for the date filter in list tables . default is ‘Filter by date’.
			'filter_by_date'           => $this->str_name_plural . ': Filter by date',

			// String for the table pagination hidden heading.
			'items_list_navigation'    => $this->str_name_plural . ' list navigation',

			// String for the table hidden heading.
			'items_list'               => $this->str_name_plural . ' list',

			/*
				* ===========================================
				* There are five additional labels have been made available for custom post types since wordpress 5.0
				* ===========================================
				*/

			// The label used in the editor notice after publishing a post. Default “Post published.” / “Page published.” .
			'item_published'           => $this->str_name_singular . ' published',

			// The label used in the editor notice after publishing a private post. Default “Post published privately.” / “Page published privately.” .
			'item_published_privately' => $this->str_name_singular . ' published privately',

			// The label used in the editor notice after reverting a post to draft. Default “Post reverted to draft.” / “Page reverted to draft.” .
			'item_reverted_to_draft'   => $this->str_name_singular . ' reverted to draft',

			// The label used in the editor notice after scheduling a post to be published at a later date. Default “Post scheduled.” / “Page scheduled.” .
			'item_scheduled'           => $this->str_name_singular . ' scheduled',

			// The label used in the editor notice after updating a post. Default “Post updated.” / “Page updated.” .
			'item_updated'             => $this->str_name_singular . ' updated',

			// Title for a navigation link block variation . default is ‘Post Link’ / ‘Page Link’.
			'item_link'                => $this->str_name_singular . ' Link',

			// Description for a navigation link block variation . default is 'A link to a post' / 'A link to a page'.
			'item_link_description'    => 'Link to a ' . $this->str_name_singular,
		);

		return $arr_labels;
	}
}
